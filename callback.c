#include "xexpress.h"

/*
 close window when user hits the window "X" button
 William Doyle
*/

void xexpress_callback(GtkWidget * widget, gpointer data){
	gtk_main_quit();
	return;
}
