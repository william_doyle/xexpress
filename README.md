Compile Guide:
	Prerequsits:
		sudo apt-get install libgtk2.0-dev
	
	Compilation:
		gcc main.c callback.c connect.c  xstatus.c xlist.c set_choice.c check_box.c  $(pkg-config --cflags --libs gtk+-2.0) -o xexpress
		
		
		
		

Usage guide:


To Add:
	[]	fix list to have no empty rows at any time
	[]	fix issue where short list doesn't always display properly
	[]	fix list to not include values that are not actual server options
	[]	fix bad practices
		->[]	use regex instead of blind string cutting
	[]	flag indicators to show which country is which
	[x]	make connectBTN red or green depending on connection state
	[x]	check box to hide non-recomended options
