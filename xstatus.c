#include "xexpress.h"

/*
	@author	William Doyle
	@date	January 28th 2020
	@desc	Run 'expressvpn status' and return the string result.

*/

void xstatus (char * path){
	FILE * fp;	
	system("expressvpn status > status.txt");		/* run command and pipe result to file */
	fp = fopen("status.txt", "r");	
	if (fp == NULL){								/* test file opened */
		printf("Failed to open!\n");				/* if failed to open file notify user */
		path = "STATUS NOT FOUND!";					/* put error notice in string so user will see */
		return;			
	}
	while (fgets(path, sizeof(path)*1035, fp) != NULL){	/* read from file to get status as string */
		//remove first 10 chars
		if (path[0] != 'N'){	// bad coding practice -- should probably use regex to remove junk 
			sprintf(path, "%.35s", path+10);
		}
		fclose(fp);
		return;
	}
	fclose(fp);		
};

