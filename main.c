#include "xexpress.h"
#include "countlines.h"

#define DEBUG
#ifdef DEBUG
#include "stubber.h"
#endif
/*
	@author		William Doyle
	@date		January 26th 2020
	@filename	xexpress/main.c
	@description	Interface with ExpressVPN's terminal program to provide a graphical interface 
*/


#define WWIDTH 200	
#define WINDOW_TITLE "XeXpress"		


int main(int argc, char ** argv){
	/* declare variables */
	extern GtkWidget *connectBTN;					/* connect btn */
	extern GtkWidget *serverMENU;					/* server menu */
	extern GtkWidget **sOptions;					/* server menu item */
	extern char serverNames[NUM_SERVERS][1035];
	extern char status[1035];
	extern GtkWidget *statusLBL, *choiceServerLBL;		
	extern bool has_alias, showall;
	extern GdkColor good_color, bad_color;
	GtkWidget *window;						/* window */
	GtkWidget *menubar;		
	GtkWidget *vbox;						/* vertical box */
	GtkWidget *hbox;						/* horizontal box */
	GtkWidget *selectServer;					/* server menu item */
	GtkWidget *show_allCB;

	showall = false;
	has_alias = false; 

	current_server_name = malloc(25 * sizeof(char));
	check_malloc(current_server_name, __func__);	

	sOptions = malloc(sizeof(GtkWidget*) * NUM_SERVERS );
	check_malloc(sOptions, __func__);
	
	xlist_servers(showall);						/* gather the servers and store them in the list of servers */

#ifdef DEBUG
	for (int i = 0; i < 30; i++){	/* show the servers */
		printf("%s\n", serverNames[i]);
	}
#endif

	gtk_init(&argc, &argv);						/* initialize gtk */		
	
	/* construct parts */
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);			/* construct window */
	connectBTN = gtk_button_new_with_label("Connect");		/* construct connect btn */
	gtk_window_set_title(GTK_WINDOW(window), WINDOW_TITLE);		/* set title */
	show_allCB = gtk_check_button_new_with_label("Show non-recomended servers");
	gdk_color_parse("red", &bad_color);
	gdk_color_parse("green", &good_color);
	hbox = gtk_hbox_new(FALSE, 0);					/* construct horizontal box */
	vbox = gtk_vbox_new(FALSE, 0);					/* construct vertical box */
	menubar = gtk_menu_bar_new();					/* construct menubar */
	serverMENU = gtk_menu_new();					/* construct server menu */
	selectServer = gtk_menu_item_new_with_label("SELECT SERVER");	/* construct a menu item */
	choiceServerLBL = gtk_label_new("");//set to empty string 
	
	/* establish menu system */
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(selectServer), serverMENU);
	
	gtk_container_set_border_width(GTK_CONTAINER(window), WWIDTH);			/* set window width */	
	xstatus(status);
	printf("STATUS: %s\n", status);
	statusLBL = gtk_label_new(status);				/* should also run line when status might change */
	if (status[0] != 'N'){ /* if server is connected at start make sure the button text reflects that fact */
		gtk_button_set_label(GTK_BUTTON(connectBTN), "Disconnect");
		gtk_widget_modify_fg(GTK_WIDGET(statusLBL), GTK_STATE_NORMAL, &good_color);
	}else {
		gtk_widget_modify_fg(GTK_WIDGET(statusLBL), GTK_STATE_NORMAL, &bad_color);
	}
	
	gtk_menu_shell_append(GTK_MENU_SHELL(menubar), selectServer);
	gtk_box_pack_start(GTK_BOX(vbox), menubar, FALSE, FALSE, 0);
	
	/* connect item to action */
	g_signal_connect(window, "destroy", G_CALLBACK(xexpress_callback), NULL);	/* set window action */
	g_signal_connect(GTK_OBJECT(connectBTN), "clicked", G_CALLBACK(xexpress_connect), "button");
	g_signal_connect(GTK_OBJECT(show_allCB), "clicked", G_CALLBACK(xchange_visible), NULL);
	
	make_server_menu(sOptions, serverNames, serverMENU);
	
	/* add parts to window */
	gtk_container_add(GTK_CONTAINER(hbox), vbox);
	gtk_container_add(GTK_CONTAINER(window), hbox);
	gtk_container_add(GTK_CONTAINER(hbox), statusLBL);
	gtk_container_add(GTK_CONTAINER(vbox), connectBTN);
	gtk_container_add(GTK_CONTAINER(vbox), choiceServerLBL);
	gtk_container_add(GTK_CONTAINER(vbox), show_allCB);

	/* display window */
	gtk_widget_show_all(window);				/* display all */
	gtk_main();						/* start gtk */

#ifdef DEBUG
	printf("Executing past GTK return!\n");
#endif
	/* end execution */
	free(current_server_name);
	free(sOptions);
	return EXIT_SUCCESS;
};
