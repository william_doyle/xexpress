#include "xexpress.h"

/* 
 	@author William Doyle
	@date January 28th 2020
	@filename connect.c
	@desc	one function. xexpress_connect... is run whenever connectBTN is pressed.
*/	
void xexpress_connect(GtkWidget * widget, gpointer data){
	extern char * current_server_name;
	extern char status[1035];
	extern GtkWidget * statusLBL;
	extern GtkWidget * connectBTN;
	extern bool has_alias;
	extern char chAlias[10];
	extern GdkColor good_color;
	extern GdkColor bad_color;

	if (status[0] == 'N'){ /* if button pressed while not connected */
		if (has_alias){
			char command[35];
			sprintf(command, "%s %s", "expressvpn connect ", chAlias);  
			system(command);
		}else{
			system("expressvpn connect");
		}
		system("expressvpn status > status.txt");
		xstatus(status);
		printf("%s\n", status);
		gtk_label_set_text(GTK_LABEL(statusLBL), status);
		gtk_widget_modify_fg(GTK_WIDGET(statusLBL), GTK_STATE_NORMAL, &good_color);
		gtk_button_set_label(GTK_BUTTON(connectBTN), "Disconnect");
	} else { /* button pressed while connected */
		system("expressvpn disconnect");
		system("expressvpn status > status.txt");
		xstatus(status);
		printf("%s\n", status);
		gtk_label_set_text(GTK_LABEL(statusLBL), status);
		gtk_widget_modify_fg(GTK_WIDGET(statusLBL), GTK_STATE_NORMAL, &bad_color);
		gtk_button_set_label(GTK_BUTTON(connectBTN), "Connect");

	}
};
