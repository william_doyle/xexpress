#include "xexpress.h"

// what to do when the show_allRB radio button is changed

void xchange_visible(GtkWidget * prssd, gpointer data){
	extern bool showall;
	extern GtkWidget *serverMENU;	
	extern GtkWidget **sOptions;
//	extern int num_servers;
	extern char serverNames[NUM_SERVERS][1035];
	printf("Check button changed\n");
	showall = !showall;
	xlist_servers(showall);
	for (int i = 0; i < NUM_SERVERS; i++){		// construct server choices
		gtk_menu_item_set_label(GTK_MENU_ITEM(sOptions[i]), serverNames[i]);	
		
		if (( i > NUM_RECOMENDED_SERVERS ) && (showall == false)){
			if (is_server_option(serverNames[i])/*sOptions[i] is an option according to regex*/){
				gtk_menu_item_set_label(GTK_MENU_ITEM(sOptions[i]), "");
			}
		}	
	}
	

}
