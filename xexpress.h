#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <gtk/gtk.h>
#include "check_malloc.h"
#include <ctype.h>
#include "rmatch.h"
#include <assert.h>

#define NUM_RECOMENDED_SERVERS 25
#define NUM_SERVERS 160
#define NUM_TOTAL_SERVERS NUM_SERVERS

/* globals */
GtkWidget *serverMENU;		
GtkWidget **sOptions;
bool showall;
char status[1035];
char * current_server_name;
char serverNames[NUM_SERVERS][1035];
//int num_servers;
GtkWidget *statusLBL;
GtkWidget *connectBTN;
char chAlias[10];
bool has_alias;
GtkWidget *choiceServerLBL;
GdkColor good_color;
GdkColor bad_color;
/* structs */

/* functions */
void xexpress_callback(GtkWidget*, gpointer);
void xexpress_connect(GtkWidget*,gpointer);
void xstatus(char *);
void xlist_servers(bool);
void set_choice(GtkWidget *, gpointer);
void xchange_visible(GtkWidget *, gpointer);

static inline bool is_server_option(char * line){
	// if line passes regex test to say it is a server option return true. Otherwise return false
	const char * server_pattern = "([A-Z]|[a-z])"; // " its not a server if it has no letters in it"
	if (rmatch(line, server_pattern)){
		return true;
	}
	return false;
}

static inline void make_server_menu(GtkWidget **sOptions, char serverNames[NUM_SERVERS][1035], GtkWidget * serverMENU){
	for (int i = 0; i < NUM_SERVERS; i++){
		if (is_server_option(serverNames[i])){ // recent changes here
			sOptions[i] = gtk_menu_item_new_with_label(serverNames[i]); 				/* make menu item with server name as label */
			gtk_menu_shell_append(GTK_MENU_SHELL(serverMENU), sOptions[i]); 			/* put menu item on menu */
			g_signal_connect(GTK_OBJECT(sOptions[i]), "activate", G_CALLBACK(set_choice), NULL);	/* tie menue item to behaviour */
		}
	}
};


