#pragma once
#include <regex.h>
#include <stdio.h>

static inline int rmatch(const char * string, const char * pattern){
	
	regex_t re;
	int ret;

	if (regcomp(&re, pattern, REG_EXTENDED) != 0){		/* compile regular expression and test success */
		fprintf(stderr, "%s is not a valid pattern!\n", pattern);
		return 0;
	}

	ret = regexec(&re, string, (size_t)0, NULL, 0);		/* execute regular expression */
	regfree(&re);						/* free memory used by regular expression */
	
	return (ret == 0)? 1:0 ;
};
