#include "xexpress.h"

/*
	@author	William Doyle
	@date	January 28th 2020
	@desc	Run 'expressvpn list' and return the string result.

*/

void xlist_servers (bool get_all){
	extern char serverNames[NUM_SERVERS][1035];
	FILE * fp;	

	if (get_all)
		system("expressvpn list all  > servers.txt");
	else 
		system("expressvpn list  > servers.txt");

	fp = fopen("servers.txt", "r");		
	if (fp == NULL){		
		printf("Failed to open!\n");
		for (int i = 0; i < 30; i++){
			printf("STUB i is %d\n", i);
			strcpy(serverNames[i], "NO SERVER FOUND");
		}
		return;		
	}
	char temp[1035];

	int pos = 0;
	while (fgets(temp, sizeof(temp)*1035, fp) != NULL){
		strcpy(serverNames[pos], temp);
		pos++;
	}
	fclose(fp);					
};

