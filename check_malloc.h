#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

/*
 	guide:
	call check_malloc(pointer_you_want_to_check, __func__);
*/
static inline int check_malloc(void * value, const char * funk){
	if (value){
#ifdef DEBUG
		printf("Malloc Passed\n");
#endif
		return 1;
	}else {	/* malloc failed */
		time_t now;
		time(&now);
		FILE * fp = fopen("MALLOC_LOG.txt", "a");
		fprintf(fp, "Malloc failed! Function: %s Time: %s\n", funk, ctime(&now));
		fclose(fp);
	}
	return 0;
};
