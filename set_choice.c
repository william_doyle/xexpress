#include "xexpress.h"
/*
	when the user selects an item from the list. extract the alias and 
	set it to chAlias
*/

void set_choice(GtkWidget * _pressed, gpointer data){
	
	extern char chAlias[10];
	extern bool has_alias;
	extern GtkWidget *choiceServerLBL;
	has_alias = true;
#ifdef DEBUG
	printf("%s CALLED! BTN LABEL is %s\n", __func__, gtk_menu_item_get_label(GTK_MENU_ITEM(_pressed)));
#endif
	for (int i = 0; i < 10; i++){
		chAlias[i] = '\0';
	}
	for (int i = 0; i < 10; i++){
		if ((isalpha(gtk_menu_item_get_label(GTK_MENU_ITEM(_pressed))[i])) || (isdigit(gtk_menu_item_get_label(GTK_MENU_ITEM(_pressed))[i])) ){
			chAlias[i] = gtk_menu_item_get_label(GTK_MENU_ITEM(_pressed))[i];
		}
		else{
			break;
		}
	}
	printf("ALIAS is: %s\n", chAlias);
/*
	char _alias[25];
	char _country[25];
	char _location[25];
	char _recomended[25];
	sscanf(	gtk_label_get_label(GTK_LABEL(choiceServerLBL)), "%s %s %s %s", _alias, _country, _location, _recomended);
	printf("%6s %s %s %s\n", _alias, _country, _location, _recomended);
	gtk_label_set_label(GTK_LABEL(choiceServerLBL), _location);
*/

	gtk_label_set_label(GTK_LABEL(choiceServerLBL), gtk_menu_item_get_label(GTK_MENU_ITEM(_pressed)) );
};
