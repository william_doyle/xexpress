#pragma once
#include <stdio.h>
#include <stdlib.h>

static inline int countlines(char * fname){
	FILE * fp = fopen(fname, "r");
	long linecount = 0;
	char c;
	for (c = getc(fp); c != EOF; c = getc(fp)){
		if (c == '\n'){
			linecount++;
		}
	}
	fclose(fp);
	return linecount;
};
